# o toki ala e ni
MAKEFLAGS += --no-print-directory

DATESTAMP := $(shell date --iso-8601)

# Main project directory
# POKI_LAWA = .
ifneq ($(wildcard ../../.gitmodules),)
# $(info Found ../../.gitmodules)
# $(info $(shell grep "nasin/doukutsu-rs$$" "../../.gitmodules"))
ifeq ($(shell grep "nasin/doukutsu-rs$$" "../../.gitmodules"),)
POKI_LAWA = .
else
POKI_LAWA = ../..
endif

else
# $(info Could not find ../../.gitmodules)
POKI_LAWA = .
endif

# Translations
POKI_ANTE = $(POKI_LAWA)/ante

# Source game
MUSI_NANPA_WAN_PI_TOKI_NIJON = $(POKI_LAWA)/doukutsu
MUSI_NANPA_WAN_PI_TOKI_INLI  = $(POKI_LAWA)/CaveStory
MUSI_NANPA_WAN               = $(MUSI_NANPA_WAN_PI_TOKI_INLI)

# doukutsu-rs executable
MUSI_NANPA_TU = doukutsu-rs.x86_64.elf#		Linux-xx64
# MUSI_NANPA_TU = doukutsu-rs_mac-arm64.app#	Mac-arm64
# MUSI_NANPA_TU = doukutsu-rs_mac-x64.app#	Mac-x64
# MUSI_NANPA_TU = doukutsu-rs.i686.exe#		Windows-x32
# MUSI_NANPA_TU = doukutsu-rs.x86_64.exe	Windows-x64

# Build directory
POKI_PALI_SITELEN_LASINA  = $(POKI_LAWA)/pali/musi-Tokusu-pi-sitelen-Lasina
POKI_PALI_SITELEN_ILAKANA = $(POKI_LAWA)/pali/musi-Tokusu-pi-sitelen-Ilakana
POKI_PALI_SITELEN_PONA    = $(POKI_LAWA)/pali/musi-Tokusu-pi-sitelen-pona

POKI_PALI                      = $(POKI_LAWA)/pali/musi-Tokusu-RS
POKI_PALI_SITELEN_LASINA       = $(POKI_PALI)/data/sitelen-Lasina/mod
POKI_PALI_SITELEN_ILAKANA      = $(POKI_PALI)/data/sitelen-Ilakana/mod
POKI_PALI_SITELEN_ILAKANA_SJIS = $(POKI_PALI)/data/sitelen-Ilakana-SJIS/mod
POKI_PALI_SITELEN_PONA         = $(POKI_PALI)/data/sitelen-pona/mod

# Dist directory
LIPU_PINI                      = musi-Tokusu-RS-$(DATESTAMP).zip
POKI_PINI                      = $(POKI_LAWA)/pini
POKI_PINI_SITELEN_LASINA       = $(POKI_PINI)/musi-Tokusu-RS/data/sitelen-Lasina/mod
POKI_PINI_SITELEN_ILAKANA      = $(POKI_PINI)/musi-Tokusu-RS/data/sitelen-Ilakana/mod
POKI_PINI_SITELEN_ILAKANA_SJIS = $(POKI_PINI)/musi-Tokusu-RS/data/sitelen-Ilakana-SJIS/mod
POKI_PINI_SITELEN_PONA         = $(POKI_PINI)/musi-Tokusu-RS/data/sitelen-pona/mod

# Use `make LANG=` to make all translations (except sitelen-Ilakana-SJIS)
# Use `make LANG=en` for sitelen-Lasina only
# Use `make LANG=tok` for sitelen-pona only
# Use `make LANG=jp_JP.UTF-8` for sitelen-Ilakana only
# Use `make LANG=ja_JP.SJIS` for sitelen-Ilakana-SJIS only
# Use `make LANG=ja` for both sitelen-Ilakana and sitelen-Ilakana-SJIS
# Default behavior is locale dependent
ifneq ($(filter en%, $(LANG)), )
	PALI_TARGETS = $(POKI_PALI_SITELEN_LASINA)
	PINI_TARGETS = $(POKI_PINI_SITELEN_LASINA)
else ifneq ($(filter jp%UTF-8, $(LANG)), )
	PALI_TARGETS = $(POKI_PALI_SITELEN_ILAKANA)
	PINI_TARGETS = $(POKI_PINI_SITELEN_ILAKANA)
else ifneq ($(filter jp%SJIS, $(LANG)), )
	PALI_TARGETS = $(POKI_PALI_SITELEN_ILAKANA_SJIS)
	PINI_TARGETS = $(POKI_PINI_SITELEN_ILAKANA_SJIS)
else ifneq ($(filter jp%, $(LANG)), )
	PALI_TARGETS = $(POKI_PALI_SITELEN_ILAKANA) $(POKI_PALI_SITELEN_ILAKANA_SJIS)
	PINI_TARGETS = $(POKI_PINI_SITELEN_ILAKANA) $(POKI_PINI_SITELEN_ILAKANA_SJIS)
else ifneq ($(filter tok%, $(LANG)), )
	PALI_TARGETS = $(POKI_PALI_SITELEN_PONA)
	PINI_TARGETS = $(POKI_PINI_SITELEN_PONA)
else
	PALI_TARGETS = $(POKI_PALI_SITELEN_LASINA) $(POKI_PALI_SITELEN_ILAKANA) $(POKI_PALI_SITELEN_ILAKANA_SJIS) $(POKI_PALI_SITELEN_PONA)
	PINI_TARGETS = $(POKI_PINI_SITELEN_LASINA) $(POKI_PINI_SITELEN_ILAKANA) $(POKI_PINI_SITELEN_ILAKANA_SJIS) $(POKI_PINI_SITELEN_PONA)
endif

# Download original game (default AGTP English Patched)
ifneq ($(filter jp%, $(LANG)), )
	MUSI_NANPA_WAN = $(POKI_LAWA)/doukutsu
else 
	MUSI_NANPA_WAN = $(POKI_LAWA)/CaveStory
endif

.PHONY: all       ale ali  \
	clean     weka	   \
	build     pali     \
	dist      pini     \
	install   pana     \
	uninstall weka     \
	run       musi $(SITELEN_ILAKANA_TARGETS)



all: pali
ale: pali
ali: pali



build: pali
pali: $(PALI_TARGETS)

# Base game files
$(POKI_PALI): $(POKI_PALI)/data $(POKI_PALI)/README.md $(POKI_PALI)/README.en.md | $(POKI_PALI)/ $(MUSI_NANPA_WAN)
	rsync -ruE --chmod=Fu=rw,Fg=r,Fo=r $(MUSI_NANPA_WAN)/* $(POKI_PALI)
$(POKI_PALI)/:
	mkdir -p $@
$(POKI_PALI)/data/mods.txt: $(POKI_PALI)/data/
	cp lipu/mods.txt $@
$(POKI_PALI)/data: $(POKI_PALI)/data/mods.txt $(POKI_PALI)/data/csfont.fnt $(POKI_PALI)/data/csfont_0.png $(POKI_PALI)/data/Npc $(POKI_PALI)/data/Caret.pbm $(POKI_PALI)/data/Title.pbm $(POKI_PALI)/data/TextBox.pbm $(POKI_PALI)/data/locale | $(POKI_PALI)/data/
$(POKI_PALI)/data/: | $(POKI_PALI)/
	mkdir -p $@
$(POKI_PALI)/data/csfont.fnt: | $(POKI_PALI)/data/
	cp lipu/csfont.fnt $@
$(POKI_PALI)/data/csfont_0.png: | $(POKI_PALI)/data/
	cp lipu/csfont_0.png $@
$(POKI_PALI)/README.md: README.md
	cp README.md $@
$(POKI_PALI)/README.en.md: README.en.md
	cp README.en.md $@

# Overwrite images
$(POKI_PALI)/data/Caret.pbm: $(POKI_PALI)/data/ | $(POKI_ANTE)/ale
	cp $(POKI_ANTE)/ale/data/Caret.pbm $@
$(POKI_PALI)/data/TextBox.pbm: $(POKI_PALI)/data/ | $(POKI_ANTE)/ale
	cp $(POKI_ANTE)/ale/data/TextBox.pbm $@
$(POKI_PALI)/data/Title.pbm: $(POKI_PALI)/data/ | $(POKI_ANTE)/sitelen-pona
	cp $(POKI_ANTE)/sitelen-pona/data/Title.pbm $@
$(POKI_PALI)/data/Npc: $(POKI_PALI)/data/Npc/NpcSym.pbm
$(POKI_PALI)/data/Npc/NpcSym.pbm: $(POKI_PALI)/data/Npc/ | $(POKI_ANTE)/ale
	cp $(POKI_ANTE)/ale/data/Npc/NpcSym.pbm $@
$(POKI_PALI)/data/Npc/:
	mkdir -p $@

# locale
$(POKI_PALI)/data/locale: $(POKI_PALI)/data/locale/tok_sitelen-Ilakana.json $(POKI_PALI)/data/locale/tok_sitelen-Lasina.json $(POKI_PALI)/data/locale/tok_sitelen-pona.json | $(POKI_PALI)/data/locale/
$(POKI_PALI)/data/locale/tok_sitelen-Ilakana.json: | $(POKI_PALI)/data/locale/
	cp lipu/locale/tok_sitelen-Ilakana.json $@
$(POKI_PALI)/data/locale/tok_sitelen-Lasina.json: | $(POKI_PALI)/data/locale/
	cp lipu/locale/tok_sitelen-Lasina.json $@
$(POKI_PALI)/data/locale/tok_sitelen-pona.json: | $(POKI_PALI)/data/locale/
	cp lipu/locale/tok_sitelen-pona.json $@
$(POKI_PALI)/data/locale/:
	mkdir -p $@

# sitelen Lasina mod
$(POKI_PALI_SITELEN_LASINA): $(POKI_ANTE)/toki-pona $(POKI_PALI) $(POKI_PALI_SITELEN_LASINA)/mod.txt $(POKI_PALI_SITELEN_LASINA)/stage.tbl | $(POKI_PALI_SITELEN_LASINA)/
	cp -r $(POKI_ANTE)/toki-pona/data/* $(POKI_PALI_SITELEN_LASINA)
$(POKI_PALI_SITELEN_LASINA)/mod.txt: | $(POKI_PALI_SITELEN_LASINA)/
	cp lipu/sitelen-Lasina/mod/mod.txt $@
$(POKI_PALI_SITELEN_LASINA)/stage.tbl: | $(POKI_PALI_SITELEN_LASINA)/
	cp $(POKI_ANTE)/toki-pona/data/stage.tbl $@
$(POKI_PALI_SITELEN_LASINA)/:
	mkdir -p $@

# sitelen pona mod
$(POKI_PALI_SITELEN_PONA): $(POKI_ANTE)/sitelen-pona $(POKI_PALI) $(POKI_PALI_SITELEN_PONA)/mod.txt $(POKI_PALI_SITELEN_LASINA)/stage.tbl | $(POKI_PALI_SITELEN_PONA)/
	cp -r $(POKI_ANTE)/sitelen-pona/data/* $(POKI_PALI_SITELEN_PONA)
$(POKI_PALI_SITELEN_PONA)/mod.txt: | $(POKI_PALI_SITELEN_PONA)/
	cp lipu/sitelen-pona/mod/mod.txt $@
$(POKI_PALI_SITELEN_PONA)/stage.tbl: | $(POKI_PALI_SITELEN_LASINA)/
	cp $(POKI_ANTE)/sitelen-pona/data/stage.tbl $@
$(POKI_PALI_SITELEN_PONA)/:
	mkdir -p $@

# sitelen Ilakana mod
$(POKI_PALI_SITELEN_ILAKANA): $(POKI_ANTE)/sitelen-Ilakana $(POKI_PALI) $(POKI_PALI_SITELEN_ILAKANA)/mod.txt $(POKI_PALI_SITELEN_ILAKANA)/stage.tbl | $(POKI_PALI_SITELEN_ILAKANA)/
	cp -r $(POKI_ANTE)/sitelen-Ilakana/data/* $(POKI_PALI_SITELEN_ILAKANA)
$(POKI_PALI_SITELEN_ILAKANA)/mod.txt: | $(POKI_PALI_SITELEN_ILAKANA)/
	cp lipu/sitelen-Ilakana/mod/mod.txt $@
$(POKI_PALI_SITELEN_ILAKANA)/stage.tbl: | $(POKI_PALI_SITELEN_ILAKANA)/
	cp $(POKI_ANTE)/sitelen-Ilakana-SJIS/data/stage.tbl $@
$(POKI_PALI_SITELEN_ILAKANA)/:
	mkdir -p $@

# sitelen Ilakana SJIS mod
$(POKI_PALI_SITELEN_ILAKANA_SJIS): $(POKI_ANTE)/sitelen-Ilakana-SJIS $(POKI_PALI) $(POKI_PALI_SITELEN_ILAKANA_SJIS)/mod.txt $(POKI_PALI_SITELEN_ILAKANA_SJIS)/stage.tbl | $(POKI_PALI_SITELEN_ILAKANA_SJIS)/ 
	cp -r $(POKI_ANTE)/sitelen-Ilakana-SJIS/data/* $(POKI_PALI_SITELEN_ILAKANA_SJIS)
$(POKI_PALI_SITELEN_ILAKANA_SJIS)/mod.txt: | $(POKI_PALI_SITELEN_ILAKANA_SJIS)/
	cp lipu/sitelen-Ilakana-SJIS/mod/mod.txt $@
$(POKI_PALI_SITELEN_ILAKANA_SJIS)/stage.tbl: | $(POKI_PALI_SITELEN_ILAKANA_SJIS)/
	cp $(POKI_ANTE)/sitelen-Ilakana-SJIS/data/stage.tbl $@
$(POKI_PALI_SITELEN_ILAKANA_SJIS)/:
	mkdir -p $@



ante/toki-pona:
	git submodule update --init --rebase --remote
ante/sitelen-Ilakana:
	git submodule update --init --rebase --remote
ante/sitelen-Ilakana-SJIS:
	git submodule update --init --rebase --remote
ante/sitelen-pona:
	git submodule update --init --rebase --remote



run: musi
musi: $(POKI_PALI)/$(MUSI_NANPA_TU)
	@cd $(POKI_PALI); ./$(MUSI_NANPA_TU)



dist: pini
pini: pali $(POKI_PINI)/$(LIPU_PINI) 
$(POKI_PINI)/$(LIPU_PINI): $(POKI_PINI)/
ifdef ($@)
	rm -f $@
endif
	cd $(POKI_PALI)/; zip -r $(LIPU_PINI) * 1> /dev/null
	mv $(POKI_PALI)/$(LIPU_PINI) $(POKI_PINI)/$(LIPU_PINI)
	@echo 'pini pona'
$(POKI_PINI)/:
	mkdir -p $@



clean: weka
weka:
	git submodule deinit --all
	rm -rf $(POKI_PALI)
	rm -rf $(POKI_PINI)/$(LIPU_PINI)

# doukutsu-rs builds
# https://nightly.link/doukutsu-rs/doukutsu-rs/workflows/ci/master?preview
# Good luck getting Android build to work...
# https://github.com/doukutsu-rs/doukutsu-rs?tab=readme-ov-file#data-files
doukutsu-rs_android.zip:
	wget 'https://nightly.link/doukutsu-rs/doukutsu-rs/workflows/ci/master/doukutsu-rs_android.zip'

doukutsu-rs_linux-x64.zip:
	wget 'https://nightly.link/doukutsu-rs/doukutsu-rs/workflows/ci/master/doukutsu-rs_linux-x64.zip'

$(POKI_PALI)/doukutsu-rs.x86_64.elf: | doukutsu-rs_linux-x64.zip
	unzip doukutsu-rs_linux-x64.zip -d $(POKI_PALI) 1> /dev/null;
	chmod 744 $@

doukutsu-rs_mac-arm64.zip:
	wget 'https://nightly.link/doukutsu-rs/doukutsu-rs/workflows/ci/master/doukutsu-rs_mac-arm64.zip'
$(POKI_PALI_SITELEN_LASINA)/doukutsu-rs_mac-arm64.app: | doukutsu-rs_mac-arm64.zip
	unzip doukutsu-rs_mac-arm64.zip -d $(POKI_PALI_SITELEN_LASINA) 1> /dev/null
	mv $(POKI_PALI_SITELEN_LASINA)/doukutsu-rs.app $@
	chmod 744 $@
$(POKI_PALI_SITELEN_ILAKANA)/doukutsu-rs_mac-arm64.app: | doukutsu-rs_mac-arm64.zip
	unzip doukutsu-rs_mac-arm64.zip -d $(POKI_PALI_SITELEN_ILAKANA) 1> /dev/null
	mv $(POKI_PALI_SITELEN_ILAKANA)/doukutsu-rs.app $@
	chmod 744 $@

doukutsu-rs_mac-x64.zip:
	wget 'https://nightly.link/doukutsu-rs/doukutsu-rs/workflows/ci/master/doukutsu-rs_mac-x64.zip'
$(POKI_PALI_SITELEN_LASINA)/doukutsu-rs_mac-x64.app: | doukutsu-rs_mac-x64.zip
	unzip doukutsu-rs_mac-arm64.zip -d $(POKI_PALI_SITELEN_LASINA) 1> /dev/null
	mv $(POKI_PALI_SITELEN_LASINA)/doukutsu-rs.app $@
	chmod 744 $@
$(POKI_PALI_SITELEN_ILAKANA)/doukutsu-rs_mac-x64.app: | doukutsu-rs_mac-x64.zip
	unzip doukutsu-rs_mac-arm64.zip -d $(POKI_PALI_SITELEN_ILAKANA) 1> /dev/null
	mv $(POKI_PALI_SITELEN_ILAKANA)/doukutsu-rs.app $@
	chmod 744 $@

doukutsu-rs_windows-x32.zip:
	wget 'https://nightly.link/doukutsu-rs/doukutsu-rs/workflows/ci/master/doukutsu-rs_windows-x32.zip'
$(POKI_PALI_SITELEN_LASINA)/doukutsu-rs.i686.exe: | doukutsu-rs_windows-x32.zip
	unzip doukutsu-rs_windows-x32.zip -d $(POKI_PALI_SITELEN_LASINA) 1> /dev/null; chmod 744 $@
$(POKI_PALI_SITELEN_ILAKANA)/doukutsu-rs.i686.exe: | doukutsu-rs_windows-x32.zip
	unzip doukutsu-rs_windows-x32.zip -d $(POKI_PALI_SITELEN_LASINA) 1> /dev/null; chmod 744 $@

doukutsu-rs_windows-x64.zip:
	wget 'https://nightly.link/doukutsu-rs/doukutsu-rs/workflows/ci/master/doukutsu-rs_windows-x64.zip'
$(POKI_PALI_SITELEN_LASINA)/doukutsu-rs.x86_64.exe: | doukutsu-rs_windows-x64.zip
	unzip doukutsu-rs_windows-x64.zip -d $(POKI_PALI_SITELEN_LASINA) 1> /dev/null; chmod 744 $@
$(POKI_PALI_SITELEN_ILAKANA)/doukutsu-rs.x86_64.exe: | doukutsu-rs_windows-x64.zip
	unzip doukutsu-rs_windows-x64.zip -d $(POKI_PALI_SITELEN_ILAKANA) 1> /dev/null; chmod 744 $@


$(POKI_LAWA)/doukutsu: | $(POKI_LAWA)/dou_1006.zip
	@cd $(POKI_LAWA); unzip 'dou_1006.zip' 1> /dev/null

$(POKI_LAWA)/dou_1006.zip: | $(POKI_LAWA)
	@cd $(POKI_LAWA); wget 'https://studiopixel.jp/binaries/dou_1006.zip' 1> /dev/null

$(POKI_LAWA)/CaveStory: | $(POKI_LAWA)/cavestoryen.zip
	@cd $(POKI_LAWA); unzip 'cavestoryen.zip' 1> /dev/null

$(POKI_LAWA)/cavestoryen.zip: | $(POKI_LAWA)
	@cd $(POKI_LAWA); wget 'https://www.cavestory.org/downloads/cavestoryen.zip' 1> /dev/null
