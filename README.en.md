# doukutsu-rs
Translation mod-pack for doukutsu-rs

sitelen Lasina en sitelen Ilakana en sitelen pona li lon.

[README.en.md (English)](README.en.md)

README.jp.md (日本語)

[![Latest Release](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/badges/release.svg)](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/releases)

## make install

```
git clone https://gitlab.com/lupa-Tokusu-Monokatali/doukutsu-rs.git
cd doukutsu-rs
```

Use one of the following:

- `make install LANG=en` sitelen Lasina only
- `make install LANG=ja_JP.UTF8` sitelen Ilakana Shift-JIS only
- `make install LANG=ja_JP.SJIS` sitelen Ilakana Unicode only
- `make install LANG=jp` sitelen Ilakana (SJIS and UTF8)
- `make install LANG=tp` sitelen pona only
- `make install LANG=` all

Run: `make run`

## Manual Installation

Download [doukutsu-rs](https://github.com/doukutsu-rs/doukutsu-rs/releases)

Move the executable into `musi-Tokusu-RS`

`musi-Tokusu-RS` should look like the following:

```
.
├── Config.dat
├── data
├── DoConfig.exe
├── Doukutsu.exe
├── doukutsu-rs.x86_64.elf
├── Manual
├── Manual.html
└── OrgView.exe
```

## Set language

![](sitelen/doukutsu-rs-lang-01.png)
Go to "Options"

![](sitelen/doukutsu-rs-lang-02.png)
Go to "Language"

![](sitelen/doukutsu-rs-lang-03.png)
Select a language.

![](sitelen/doukutsu-rs-lang-04.png)
Go to "musi ante"

![](sitelen/doukutsu-rs-lang-05.png)
Select the language mod.
