# doukutsu-rs
toki ante pi nasin doukutsu-rs

sitelen Lasina en sitelen Ilakana en sitelen pona li lon.

[README.en.md (English)](README.en.md)

README.jp.md (日本語)

[![Latest Release](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/badges/release.svg)](https://gitlab.com/lupa-Tokusu-Monokatali/musi-Tokusu-Monokatali/-/releases)

## make pana

```
git clone https://gitlab.com/lupa-Tokusu-Monokatali/doukutsu-rs.git
cd doukutsu-rs
```

o kepeken wan e ni:

- `make pana LANG=en` sitelen Lasina taso
- `make pana LANG=ja_JP.UTF8` sitelen Ilakana Shift-JIS taso
- `make pana LANG=ja_JP.SJIS` sitelen Ilakana UTF8 taso
- `make pana LANG=jp` sitelen Ilakana ale (nanpa Shift-JIS en nanpa UTF8)
- `make pana LANG=tp` sitelen pona taso
- `make pana LANG=` ale

o `make musi`

## pana luka

o kama jo e musi [doukutsu-rs](https://github.com/doukutsu-rs/doukutsu-rs/releases)

o pana e ona lon poki `musi-Tokusu-RS`

poki `musi-Tokusu-RS` li wile sama lukin e ni:

```
.
├── Config.dat
├── data
├── DoConfig.exe
├── Doukutsu.exe
├── doukutsu-rs.elf (Linux)
├── doukutsu-rs.app (MacOS)
├── doukutsu-rs.exe (Windows)
├── Manual
├── Manual.html
└── OrgView.exe
```

## o ante e toki

![](sitelen/doukutsu-rs-lang-01.png)

o open e "Options"



![](sitelen/doukutsu-rs-lang-02.png)

o open e "Language".



![](sitelen/doukutsu-rs-lang-03.png)

o ante e toki.



![](sitelen/doukutsu-rs-lang-04.png)

o open e "musi ante".



![](sitelen/doukutsu-rs-lang-05.png)

o open musi kepeken toki ante.
